var rapport_2016_items = [
  {
    "src": "/uploads/images/rapport_2016/01_Pl\u00e9ni\u00e8re_1.png", 
    "w": 800, 
    "h": 480
  }, 
  {
    "src": "/uploads/images/rapport_2016/02_Pl\u00e9ni\u00e8re_2.png", 
    "w": 800, 
    "h": 480
  }, 
  {
    "src": "/uploads/images/rapport_2016/03_LegGen_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/04_LegGen_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/05_DroitsLib_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/06_DroitsLib_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/07_ComFin_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/08_ComFin_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/09_Agriculture_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/10_Agriculture_2.png", 
    "w": 800, 
    "h": 651
  }, 
  {
    "src": "/uploads/images/rapport_2016/11_Industrie_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/12_Industrie_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/13_Sant\u00e9_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/14_Sant\u00e9_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/15_Jeunesse_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/16_Jeunesse_2.png", 
    "w": 800, 
    "h": 645
  }, 
  {
    "src": "/uploads/images/rapport_2016/17_AdminArmes_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/18_AdminArmes_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/19_ComRI_1.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/20_ComRI_2.png", 
    "w": 800, 
    "h": 648
  }, 
  {
    "src": "/uploads/images/rapport_2016/21_S\u00e9cuD\u00e9f_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/22_S\u00e9cuD\u00e9f_2.png", 
    "w": 800, 
    "h": 644
  }, 
  {
    "src": "/uploads/images/rapport_2016/23_R\u00e9formeAdmin_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/24_R\u00e9formeAdmin_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/25_D\u00e9vR\u00e9gional_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/26_D\u00e9vR\u00e9gional_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/27_Martyrs_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/28_Martyrs_2.png", 
    "w": 800, 
    "h": 650
  }, 
  {
    "src": "/uploads/images/rapport_2016/29_Handicap\u00e9s_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/30_Handicap\u00e9s_2.png", 
    "w": 800, 
    "h": 648
  }, 
  {
    "src": "/uploads/images/rapport_2016/31_Femmes_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/32_Femmes_2.png", 
    "w": 800, 
    "h": 651
  }, 
  {
    "src": "/uploads/images/rapport_2016/33_TRE_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/34_TRE_2.png", 
    "w": 800, 
    "h": 654
  }, 
  {
    "src": "/uploads/images/rapport_2016/35_\u00c9lectorale_1.png", 
    "w": 800, 
    "h": 492
  }, 
  {
    "src": "/uploads/images/rapport_2016/36_\u00c9lectorale_2.png", 
    "w": 800, 
    "h": 637
  }, 
  {
    "src": "/uploads/images/rapport_2016/37_NDA_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/38_NDA_2.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/39_NDA_3.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/40_NDA_4.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/41_NDA_5.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/42_NDA_6.png", 
    "w": 800, 
    "h": 515
  }, 
  {
    "src": "/uploads/images/rapport_2016/43_Nidaa_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/44_Nidaa_2.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/45_Nidaa_3.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/46_Nidaa_4.png", 
    "w": 800, 
    "h": 679
  }, 
  {
    "src": "/uploads/images/rapport_2016/47_Nidaa_5.png", 
    "w": 800, 
    "h": 595
  }, 
  {
    "src": "/uploads/images/rapport_2016/48_AlHorra_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/49_AlHorra_2.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/50_AlHorra_3.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/51_FP_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/52_FP_2.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/53_UPL_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/54_UPL_2.png", 
    "w": 800, 
    "h": 680
  }, 
  {
    "src": "/uploads/images/rapport_2016/55_Afek_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/56_Afek_2.png", 
    "w": 800, 
    "h": 584
  }, 
  {
    "src": "/uploads/images/rapport_2016/57_SocialD\u00e9mo_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/58_SocialD\u00e9mo_2.png", 
    "w": 800, 
    "h": 584
  }, 
  {
    "src": "/uploads/images/rapport_2016/59_Aucun_1.png", 
    "w": 800, 
    "h": 491
  }, 
  {
    "src": "/uploads/images/rapport_2016/60_Aucun_2.png", 
    "w": 800, 
    "h": 680
  }
];