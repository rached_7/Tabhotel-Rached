$(document).ready(function()
{
	$('#gallery-open').click(function()
	{
		var pswpElement = document.querySelectorAll('.pswp')[0];

		// define options (if needed)
		var options = {
			// optionName: 'option value'
			// for example:
			index: 0 // start at first slide
		};

		// Initializes and opens PhotoSwipe
		var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, rapport_2016_items, options);
		gallery.init();

		return false;
	})
});