# -*- coding: utf-8 -*-

DEBUG = True

BASE_URL = 'http://www.albawsala.com'

DB_HOST = '127.0.0.1'
DB_PORT = 27017
DB_NAME = 
ADMINS = []
LANGS = {
    'ar': 'ar_TN',
    'fr': 'fr_FR',
    'en': 'en_US',
}

PAGE_MAX = 10
FILES_PAGE_MAX = 8

SECRET_KEY = '\x93\xa8\xfdJ\xa8\xf4tk\xc6\x883\x87\xa3\xab3\xe8Z\x0eO9\x0br\xfa\xdb'

MAX_CONTENT_LENGTH = 1024 * 1024 * 20

STATIC_URL = ''

UPLOAD_FOLDER = '/home/user0/www/uploads_albawsala'

UPLOADED_DOCUMENTS_ALLOW = 'pdf'
UPLOADED_DOCUMENTS_DEST = '/home/user0/www/uploads/albawsala/documents'
UPLOADED_DOCUMENTS_URL = '/uploads/documents/'

UPLOADED_IMAGES_DEST = '/home/user0/www/uploads/albawsala/images'
UPLOADED_IMAGES_URL = '/uploads/images/'

UPLOADED_THUMBNAILS_DEST = '/home/user0/www/uploads/albawsala/thumbnails'
UPLOADED_THUMBNAILS_URL = '/uploads/thumbnails/'

FB_PAGE_URL = 'https://www.facebook.com/AlBawsala'
TWITTER_URL = 'https://twitter.com/AlBawsalaTN'
