from flask import Flask, Blueprint, g, abort
try:
	from flaskext.uploads import configure_uploads
except ImportError:
	from flask_uploads import configure_uploads
from flask_babel import Babel
from common.finders import FinderModifier
import filters
import files
import views


app = Flask(__name__)
app.config.from_pyfile('conf.py')

babel = Babel(app, 'ar_TN')

@babel.localeselector
def locale_select():
    return app.config['LANGS'][g.lang_code]

configure_uploads(app, files.uploaded_images)
configure_uploads(app, files.uploaded_documents)

finder = FinderModifier(app)

app.jinja_env.filters['get_ref'] = filters.get_referenced
app.jinja_env.filters['path'] = filters.get_file_url
app.jinja_env.filters['nl2br'] = filters.nl2br
app.jinja_env.filters['option'] = filters.get_option
app.jinja_env.filters['lang'] = filters.set_lang_suffix

root = Blueprint('root', __name__)

root.add_url_rule('/', 'home', views.index)
root.add_url_rule('/pub/<pub_id>', 'publication', views.publication)
root.add_url_rule('/presse', 'presse', views.presse)
#root.add_url_rule('/equipe', 'equipe', views.equipe, defaults={'cat': 'BUR'})
#root.add_url_rule('/equipe/<cat>', 'equipe', views.equipe)
root.add_url_rule('/fb_channel', 'fb_channel', views.fb_channel)
root.add_url_rule('/<page_name>', 'page', views.page)

root.add_url_rule('/newsletter', 'newsletter', views.newsletter)
root.add_url_rule('/newsletter_confirm/<token>', 'newsletter_confirm', views.newsletter_confirm)
root.add_url_rule('/newsletter_request', 'newsletter_request', views.newsletter_request, methods=['GET','POST'])


@root.url_defaults
def add_language_code(endpoint, values):
    values.setdefault('lang_code', g.lang_code)

@root.url_value_preprocessor
def pull_lang_code(endpoint, values):
    lng = values.pop('lang_code')
    lng in app.config['LANGS'] or abort(404)
    g.lang_code = lng


app.register_blueprint(root, url_defaults={'lang_code':'fr'})
app.register_blueprint(root, url_prefix='/<lang_code>')