# -*- coding: utf-8 -*-

SITE_TITLE = 'AlBawsala.com'

REMEMBER_COOKIE_NAME = 'remember_token_albawsala'

from albawsala.conf import *

from albawsala.admin.callbacks import CALLBACKS

WIDGETS = {}