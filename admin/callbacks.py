from common import utils


def get_favicon(document, old=None):
    document['favicon'] = utils.get_favicon_url(document['url'])


def pre_save_publications(document, old=None):
    utils.get_preview(document)



CALLBACKS = {
	'pre_save':{
	    'presse': get_favicon,
	    'publications': pre_save_publications,
	},
	'post_save':{

	}
}
