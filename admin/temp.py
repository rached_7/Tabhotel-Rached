import sys
sys.path.append('/home/user0/www')

from bson.objectid import ObjectId, InvalidId
from bson.dbref import DBRef
from pymongo import MongoClient
from albawsala import conf
from common import utils
from datetime import datetime
import os
import codecs
import json

cx = MongoClient(conf.DB_HOST, conf.DB_PORT)
db = cx[conf.DB_NAME]


def execute_callback():
	for actu in db['publications'].find():
		utils.get_preview(actu, 'texte', conf.UPLOAD_FOLDER.rstrip('/uploads'))
		db['publications'].save(actu)


def touch_all():
    for col in db['collections'].find():
        db[col['_name']].update({}, {'$set': {'_meta.mtime':datetime.now()}}, multi=True)


def migrate():
	for doc in db['albawsala_documents'].find():
		obj = {
			'_id': doc['_id'],
			'type': 'DOC',
			'titre': {'fr': doc['titre'], 'ar':'', 'en':''},
			'date': doc['date'],
			'doc': doc['attachement'],
			'_meta': {'mtime': datetime.now()},
		}
		db['publications'].save(obj)

	for actu in db['albawsala_actu'].find():
		obj = {
			'_id': actu['_id'],
			'type': 'ACTU',
			'titre': {'fr': actu['titre'], 'ar':'', 'en': ''},
			'date': actu['date'],
			'texte': {'fr': actu['contenu'], 'ar':'', 'en':''},
			'_meta': {'mtime': datetime.now()},
		}
		db['publications'].save(obj)


def reset_count():
    for col in db['collections'].find():
        col['_count'] = int(db[col['_name']].count())
        db['collections'].save(col)


def rename_collections():
	names = {
		'albawsala_presse': 'presse',
		'albawsala_equipe': 'membres',
		'albawsala_pages': 'pages',
	}

	for old_, new_ in names.items():
		db[old_].rename(new_)
		db['collections'].update({'_name': old_}, {'$set': {'_name': new_}})


def regenerate_publication_thumbnails():
    from albawsala.app import app
    with app.app_context():
        for pub in db['publications'].find():
            utils.get_preview(pub, 'texte')



def migrate_subscribers():
	db_anc = cx['marsad']
	for subscriber in db_anc['subscribers'].find():
		db['subscribers'].insert(subscriber)



def resize_rapport_images():
	from PIL import Image
	dirpath = "/home/user0/Documents/rapport majles 2016"
	for filename in os.listdir(dirpath):
		if not filename.lower().endswith(".png"):
			continue
		
		filepath = "%s/%s" % (dirpath, filename)
		dstdir = "%s/out" % dirpath
		if not os.path.exists(dstdir):
			os.makedirs(dstdir)
		filedst = "%s/%s" % (dstdir, filename)

		try:
			width = 800
			im = Image.open(filepath)
			w, h = im.size
			size = width, int(h * width / w)
			im_new = im.resize(size, Image.ANTIALIAS)
			im_new.save(filedst)
			print filename
		except IOError:
			print "cannot create thumbnail for %s" % filename



def gallery_items_json():
	from PIL import Image
	dirpath = "/home/user0/www/albawsala/out"
	items = []
	for filename in sorted(os.listdir(dirpath)):
		if not filename.lower().endswith(".png"):
			continue
		
		filepath = "%s/%s" % (dirpath, filename)

		try:
			im = Image.open(filepath)
			w, h = im.size
			items.append({
				'src': '/uploads/images/rapport_2016/%s' % filename,
				'w': w,
				'h': h,
			})
			
		except IOError:
			print "cannot create thumbnail for %s" % filename

	with codecs.open('out.js', 'w', 'utf-8') as f:
		f.write(json.dumps(items, indent=2))



def add_subscribers():
	with codecs.open('/home/user0/www/albawsala/admin/new_subscribers', 'r', 'utf-8') as f:
		for line in f.read().split('\n'):
			if not db['subscribers'].find_one({'email':line}):
				db['subscribers'].save({
					'email': line,
					'active': True,
					'history': [],
				})


if __name__ == '__main__':
	add_subscribers()