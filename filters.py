from flask import g
from flask_babel import get_locale
import files
import os


def get_referenced(dbref):
    return g.db.dereference(dbref)


def get_file_url(file_id, thumb=None):
    obj = g.db.find_one('files', {'_id':file_id})

    if not obj:
        return None
    
    filename = obj["filename"]
    
    if obj['type'] == 'IMG':
        if thumb:
            filename = os.path.splitext(filename)[0] + thumb
        return files.uploaded_images.url(filename)
    
    if obj['type'] == 'DOC':
        return files.uploaded_documents.url(filename)


def nl2br(value):
    return value.replace('\n', '<br/>')


def get_option(opt, collection, path):
    collection_meta = g.db.find_one('collections', {'_name': collection})
    if not collection_meta:
        return None

    field = collection_meta.get('_schema')
    for step in path.split('.'):
        field = field[step]

    return field['_options'][opt]


def set_lang_suffix(ml_string):
    return isinstance(ml_string, dict) and ml_string.get(get_locale().language) or None
