# -*- coding: utf-8 -*-
from flask import render_template, request, url_for, redirect, g, abort
from flask_babel import gettext
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
import smtplib
import re
import uuid



is_valid_email = re.compile(r"[^@]+@[^@]+\.[^@]+")
postmaster = 'Albawsala <postmaster@albawsala.com>'
albawsala = 'AlBawsala <contact@albawsala.com>'

subscribe_message = u'''
Nous avons reçu une demande d'abonnement pour votre adresse courriel %s à la newsletter Albawsala.com.
Pour confirmer la demande, veuillez vous rendre sur la page suivante:

%s

Si vous ne souhaitez pas vous abonner à la newsletter, ignorez simplement ce message.
'''

unsubscribe_message = u'''
Nous avons reçu une demande de résiliation d'abonnement pour votre adresse courriel %s à la newsletter Albawsala.com.
Pour confirmer la demande, veuillez vous rendre sur la page suivante:

%s

Si vous ne souhaitez pas résilier votre abonnement, ignorez simplement ce message.
'''

error_message = u'''
Une erreur s'est produite lors de la vérification de votre requette, 
Veuillez vérifier l'adresse fournie ou réessayer plus tard.
'''



def sendmail(message, sender, recipient, subject):
	errors, mailServer = None, None

	msg = MIMEText(message, 'plain', 'utf-8')

	msg['Subject'] = subject
	msg['From'] = sender
	msg['To'] = recipient

	try:
		mailServer = smtplib.SMTP('localhost')
		mailServer.ehlo()
		mailServer.sendmail(sender, recipient, msg.as_string())
	except Exception as err:
		errors = err
	finally:
		if mailServer: mailServer.close()

	return errors



def subscribe(email):
	subscriber = g.db.find_one('subscribers', {'email':email}) or {}
	if subscriber.get('active'):
		return gettext(u"L'adresse email fournie est déja inscrite")

	confirm_token = uuid.uuid4().hex
	confirm_url = url_for('root.newsletter_confirm', token=confirm_token, _external=True)
	
	errors = sendmail(
		subscribe_message % (email, confirm_url),
		postmaster, email,
		gettext(u"Inscription à la newsletter AlBawsala.com")
	)

	if errors:
		return error_message

	if not subscriber:
		subscriber['email'] = email
		subscriber['active'] = False
		subscriber['history'] = []
	subscriber['token'] = confirm_token
	g.db.save('subscribers', subscriber)
	return gettext(u"Merci pour votre inscription! Un email de confirmation a été envoyé à l'adresse fournie.")



def unsubscribe(email):
	subscriber = g.db.find_one('subscribers', {'email':email})
	if not subscriber or not subscriber.get('active'):
		return gettext(u"L'adresse email fournie n'existe pas dans notre liste d'abonnés")

	confirm_token = uuid.uuid4().hex
	confirm_url = url_for('root.newsletter_confirm', token=confirm_token, _external=True)

	errors = sendmail(
		unsubscribe_message % (email, confirm_url),
		postmaster, email,
		gettext(u"Résiliation de votre abonnement à la newsletter AlBawsala.com")
	)

	if errors:
		return error_message

	g.db.update('subscribers', {'email':email}, {'$set':{'token':confirm_token}})
	return gettext(u"Un email de confirmation a été envoyé à l'adresse fournie.")



def newsletter_confirm(token):
	subscriber = g.db.find_one('subscribers', {'token':token})
	result, email = "", ""

	if not subscriber:
		result = gettext(u"Code de confirmation périmé ou invalide, Veuillez réessayer.")

	else:
		subscriber['active'] = not subscriber['active']
		subscriber['token'] = ''
		email = subscriber['email']

		if subscriber['active']:
			subscriber['history'].append(['joined', datetime.now()])
			result = gettext(u"Votre abonnement à la newsletter AlBawsala.com a été confirmé!")
			sendmail(
				u"Nouvel abonnement: %s" % email,
				postmaster, postmaster,
				u"Newsletter - nouvel abonnement"
			)
		
		else:
			subscriber['history'].append(['left', datetime.now()])
			result = gettext(u"Votre abonnement à la newsletter AlBawsala.com a été résilié!")
			sendmail(
				u"Résiliation d'abonnement: %s" % email,
				postmaster, postmaster,
				u"Newsletter - résiliation d'abonnement"
			)
		
		g.db.save('subscribers', subscriber)


	return render_template('newsletter/request.html',
		result = result,
		email = email,
	)



def newsletter_request():
	result, email = "", ""

	if request.method == 'POST':
		email = request.form.get('email')
		if not email or not is_valid_email.match(email):
			result = gettext(u'Veuillez indiquer une adresse email valide')
		elif request.form.has_key('subscribe'):
			result = subscribe(email.strip())
		elif request.form.has_key('unsubscribe'):
			result = unsubscribe(email.strip())
		else:
			abort(404)

	return render_template('newsletter/request.html',
		result = result,
		email = email,
		action = request.args.get('action', 'subscribe'),
	)



def newsletter():
	newsletter_send = request.args.get('send')
	template = 'newsletter/newsletter_5_ar.html'
	mail_html = render_template(template)

	if not newsletter_send:
		return mail_html
	
	errors, mailServer = [], None

	try:
		mailServer = smtplib.SMTP('localhost')
		mailServer.ehlo()
		sender = albawsala

		# recipients = g.db.find('subscribers', {'active':True})
		recipients = [
			{'email': 'nblxxx@gmail.com'},
			{'email': 'dhouibhayfa9@gmail.com'},
		]
		
		for subscriber in recipients:
			recipient = subscriber.get('email')
			msg = MIMEMultipart()
			msg.attach(MIMEText(mail_html, 'html', 'utf-8'))
			msg['Subject'] = u"نشرية البوصلة عدد 5"
			msg['From'] = sender
			msg['To'] = recipient
			msg.add_header('reply-to', postmaster)
			try:
				mailServer.sendmail(sender, recipient, msg.as_string())
			except (smtplib.SMTPResponseException, smtplib.SMTPRecipientsRefused) as err:
				errors.append(str(err))
	except Exception as err:
		errors.append(str(err))
	finally:
		if mailServer: mailServer.close()

	if errors:
		sendmail(
			u'\n\n'.join(errors),
			postmaster, postmaster,
			u'Newsletter mailing errors'
		)

	return "check your email"
