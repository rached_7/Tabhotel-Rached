from main import (
	index,
	publication,
	page,
	equipe,
	presse,
	fb_channel,
)

from newsletter import (
	newsletter,
	newsletter_request,
	newsletter_confirm,
)