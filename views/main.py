# -*- coding: utf-8 -*-

from flask import render_template, request, make_response, url_for, flash, redirect, g, abort
from werkzeug.exceptions import BadRequest, NotFound
from bson.objectid import ObjectId, InvalidId
from datetime import datetime



def index():
    return render_template('index.html',
        data = g.db.find_page('publications', {},
            limit = 4,
            sort =  [('date',-1),('_id',-1)],
            after = request.args.get('_after'),
            before = request.args.get('_before'),
        )
    )


def publication(pub_id):
    doc = g.db.find_one('publications', {'_id':pub_id}) or abort(404)
    if doc['type'] == 'TEMPLATE':
        return render_template("/custom/%s" % doc.get('template'), doc=doc)
    return render_template('page.html', doc = doc)


def page(page_name):
    doc = g.db.find_one('pages', {'nom':page_name}) or abort(404)
    return render_template('page.html', doc = doc)


def equipe(cat):
    liste = g.db.find('membres', {'groupe':cat}, sort=[('ordre',1)])
    
    return render_template('equipe.html',
        liste = liste,
        cat = cat,
    )


def presse():
    return render_template('presse.html',
        data = g.db.find_page('presse', {},
            limit = 8,
            sort =  [('date',-1),('_id',-1)],
            after = request.args.get('_after'),
            before = request.args.get('_before'),
        ),
    )



def fb_channel():
    n = datetime.now()
    n = n.replace(year=n.year+1)
    res = make_response('<script src="//connect.facebook.net/fr_FR/all.js"></script>')
    res.headers['Pragma'] = 'public'
    res.headers['Cache-Control'] = 'max-age=%d' % (60*60*24*365)
    res.headers['Expires'] = n.strftime('%a, %d %b %Y %H:%M:%S GMT')
    return res



